# GHC governance

This repository describes the people and processes we use to organise the GHC developer community.

It is a description of how things currently work, but should be seen as a starting point rather than holy writ. Going forward, it will live and evolve in `https://gitlab.haskell.org/ghc/ghc-hq`. We will continue to adopt new practices, subject to their feasibility and the value they bring to Haskell, GHC, and the team themselves. Your feedback and suggestions are quite welcome.

## 1. Contributing to GHC

GHC is an open source project, and we would love you to contribute to it.  For example
* You can file a bug report.
* You can help charactise or minimise a bug, so there is a small repro case.
* You can offer a patch that fixes a bug, or improves documentation
* You can review a merge request
* You can improve the user manual or documentation in the code.

[This page describes how to get started as a contributor](https://gitlab.haskell.org/ghc/ghc/-/wikis/contributing).

## 2. The GHC Team

If you want to become part of the core group that develops and maintains GHC, you can
join the **GHC Team**.   Anyone can join the team!

Being a member of the GHC Team carries power and responsibility:

**Power**
* Ability to merge patches into GHC's main trunk, subject to GHC’s code review policies. See GHC's [Contributing a Patch](https://gitlab.haskell.org/ghc/ghc/-/wikis/Contributing-a-Patch) documentation for more information about this process.
* Standing invitation to the GHC weekly meeting.

**Responsibilities**
* Actively contribute to GHC, through patches, code review, design discussions, infrastructure.  "Actively contribute" means at least with a tempo of every month or two.  e.g. making a small intervention once a year is not really enough to be a member of the team.
* Monitor, and when appropriate contribute to, the [`ghc-devs` mailing list](https://mail.haskell.org/mailman/listinfo/ghc-devs).
* Willingness to review merge requests, when the expertise fits.
* Willingness to undertake some specific obligations, to take responsibility for some specific part of GHC

### 2.1 Membership

* We keep the GHC Team membership on [a page in this repository](team.mkd).
* You can join the team by adding yourself to this list, via a pull request, along with the responsibilities you want to undertake.  The only precondition is having made a significant recent contribution; for example authoring a merge request that has landed.
* Joining the team in this way confers the ability to merge other patches.
* You can remove yourself from the team if you no longer want to be an active contributor, again via a pull request.  In the case of an inactive member, we may just move them to an Archive section at the end. Our goal is that the team list only consists of active contributors.

### 2.2 The GHC team weekly meeting

The GHC team weekly meeting lasts an hour, and offers each member of the team an opportunity to

* Hear from others what is going on; the "hum of the business".
* Describe what they are doing
* Float ideas for larger projects that they would like to do, to see how they would fit into the overall project.

Being a member of the GHC team gives you a standing invitation to attend the weekly meeting.  We might have to review this if the meeting became too large, but given people's busy-ness that seems unlikely.

## 3. GHC HQ group

People sometimes ask questions like "who ultimately makes decisions about GHC?" or "how can I get the GHC team to do X?".

As an open-source project, *decisions about GHC are primarily taken by consensus*, and all the heavy lifting is done by the GHC Team (discussed above).  However, the **GHC HQ group** exists
* as a final arbiter in case the team cannot arrive at a decision;
* to make decisions related to cross-cutting concerns with many stakeholders;
* to shepherd the overall well-being of the project;
* to oversee the list of those who have commit rights to GHC;
* to delegate (and un-delegate) authority for taking decisions in a particular area, or running a particular process (e.g. release management).

The GHC HQ group is very much a back-stop if all else fails; almost everything gets done by the team.

Note that this only applies to the GHC project and its implementation:
* Decisions about the language that GHC compiles fall in the remit of the [GHC Steering Committee](https://github.com/ghc-proposals/ghc-proposals).
* Decisions about GHC’s core library design are the territory of the [Core Libraries Committee](https://github.com/haskell/core-libraries-committee).

### 3.1 What sort of decisions does the GHC HQ group take?

Examples:

* Decide on a strategy for platform support
* Decide where to host the GHC code repository and CI
* Delegate authority to GHC team members for specific purposes (e.g. code review of particular subsystems, CI maintenance and improvements, etc)
* Decide not to decide about something.  For example, a proposal might be good in principle but we judge that we do not have the resources to evaluate it in depth.
* Resolve a technical disagreement between GHC Team members, e.g. about whether a particular merge request should be merged.
* Approve code review guidelines or coding style guidelines
* Adding new members to the GHC HQ group

###  3.2. Who is in the GHC HQ group and how does it operate?

The GHC HQ group is a small team currently consisting of

* Matthew Pickering (@mpickering)
* Ben Gamari (@bgamari)
* Simon Peyton Jones (@simonpj)

The group's charter (i.e. this document) and its decisions are held
in its repository, via its [issue tracker](https://gitlab.haskell.org/ghc/ghc-hq/-/issues).

### 3.3 How can you bring something to the attention of GHC HQ?

Anyone can ask GHC HQ to make a decision about something:
* Open an issue on the [GHC HQ repo](https://gitlab.haskell.org/ghc/ghc-hq/-/issues)
* Explain why this issue can't be resolved by the GHC Team (ghc-devs), CLC, GHC Steering Committee.
* Be clear about what action you seek, and who would have to act.

Possible responses: please ask ghc-devs, yes, no, please refine, or "we don't want to decide yes or no to that right now" (i.e a conscious non-decision).


